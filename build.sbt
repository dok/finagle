import sbt.Keys._

/*
 * Общие настройки
 */
lazy val projectSetting = Defaults.coreDefaultSettings ++ Seq(
  organization := "ru.dokwork",
  scalaVersion := "2.11.8",
  resolvers += "Twitter's Repository" at "https://maven.twttr.com/"
)


/*
 * Используемые библиотеки:
 */
val finagle = "com.twitter" %% "finagle-http" % "6.35.0"
val finagle_stats = "com.twitter" %% "finagle-stats" % "6.35.0"
val twitter_server = "com.twitter" %% "twitter-server" % "1.20.0"

/*
 * Библиотеки для тестов:
 */
val scalatest = "org.scalatest" %% "scalatest" % "3.0.0-M15"

lazy val root = (project in file("."))
  .settings(projectSetting: _*)
  .aggregate(service)

lazy val service = (project in file("service"))
  .settings(projectSetting: _*)
  .settings(Seq(
    name := "Test Service",
    version := "0.1",
    libraryDependencies ++= Seq(
      finagle,
      finagle_stats,
      twitter_server,
      scalatest % Test
    )
  ))

lazy val server = (project in file("server"))
  .settings(projectSetting: _*)
  .settings(Seq(
    name := "Client Server",
    version := "0.1",
    libraryDependencies ++= Seq(
      finagle,
      finagle_stats,
      twitter_server,
      scalatest % Test
    )
  ))