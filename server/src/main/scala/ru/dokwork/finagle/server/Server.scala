package ru.dokwork.finagle.server

import com.twitter.conversions.time._
import com.twitter.finagle.builder.ClientBuilder
import com.twitter.finagle.loadbalancer.Balancers
import com.twitter.finagle.{ Http, Service, http }
import com.twitter.server.TwitterServer
import com.twitter.util.{ Await, Future }

object Server extends TwitterServer {


  //  val balancer = Balancers.heap()
  //  val balancer = Balancers.p2c(maxEffort = 2)
  //  val balancer = Balancers.p2cPeakEwma(maxEffort = 3, decayTime = 3.seconds)
  val balancer = Balancers.aperture(
    maxEffort = 3,
    smoothWin = 3.seconds,
    lowLoad = 1.0,
    highLoad = 2.0,
    minAperture = 10)

  val client = Http.client
    .withLoadBalancer(balancer)
    .withRequestTimeout(2.seconds)
    .newService("localhost:8081,localhost:8082")


  val service = new Service[http.Request, http.Response] {
    def apply(req: http.Request): Future[http.Response] = client(req)
  }

  def main() {
    val server = Http.serve(":8080", service)
    println(s"Server started: http://localhost:8080")
    Await.ready(server)
  }
}
