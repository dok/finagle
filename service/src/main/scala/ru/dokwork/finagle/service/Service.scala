package ru.dokwork.finagle.service

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong

import com.twitter.finagle.http.Response
import com.twitter.finagle.{Http, Service, http}
import com.twitter.io.Buf.ByteArray
import com.twitter.util.{Await, Future}

object Service extends App {

  val (name, port) = parse(args)
  val timeout = new AtomicLong(0)

  def parse(args: Array[String]) = {
    if (args.length == 0 || args.length % 2 != 0) {
      throw new IllegalArgumentException(args.mkString(" "))
    }
    val params = args.filter(_.startsWith("-")).zip(args.filter(!_.startsWith("-"))).toMap
    (params.getOrElse("-n", UUID.randomUUID().toString), params.getOrElse("-p", "8080"))
  }

  val service = new Service[http.Request, http.Response] {
    def apply(req: http.Request): Future[http.Response] = Future.value({
      if (req.containsParam("timeout")) {
        timeout.set(req.getLongParam("timeout"))
      }
      println(s"go to sleep at $timeout millis")
      Thread.sleep(timeout.get())
      val response: Response = Response()
      response.content = ByteArray(s"$name\n".getBytes: _*)
      response
    })
  }
  val server = Http.serve(s":$port", service)
  println(s"Service started with name $name on port $port")
  Await.ready(server)
}
